(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "titlepage")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "url"
    "color"
    "amsmath"
    "amsfonts"
    "caption"
    "graphicx"
    "subcaption")
   (TeX-add-symbols
    '("addfig" 1)
    '("addref" 1)
    '("todo" 1))
   (LaTeX-add-labels
    "sec:problem"
    "fig:game"
    "sec:background"
    "sec:prior"
    "sec:detail"
    "fig:corner"
    "fig:net"
    "fig:input"
    "fig:hard"
    "fig:reward2")
   (LaTeX-add-bibliographies
    "ref")))

