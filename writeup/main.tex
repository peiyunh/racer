\documentclass{article}

\usepackage{url}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subcaption}

\title{Multi-agent Reinforcement Learning}
\author{Peiyun Hu, Junzhi Wu}
\date{}

\newcommand{\todo}[1]{{[\color{red}#1}]}
\newcommand{\addref}[1]{{\todo{cite:#1}}}
\newcommand{\addfig}[1]{{\todo{fig:#1}}}

\begin{document}
\maketitle
\section{Problem description} \label{sec:problem}
% problem statement
%(what your project supposed to do?)
Google DeepMind demonstrated a convolutional neural network model can
successfully learn control policies from raw sensory data using
reinforcement learning. They applied this method to seven Atari games
and found that it outperforms prior arts on six of them and even
surpasses a human expert on three of them.

Instead of Atari games, we apply a similar approach on a simpler game:
2D racing cars navigate following a suggested route while avoiding
running into walls or other cars. Every car has a same number of
distance sensors and one angle sensor. The distance sensors detect
distance of wall and other objects within its visual extent. The
angle sensor detects the angle between the moving direction and the
suggested direction. Each car decides how it moves and turns by changing
motor speeds on both side based on a pre-defined action set. Rewards
are given according to how far they move along the direction and how
well they avoid collisions. Also, instead of only one agent playing at
the same time, we have multiple agents playing in the same game
simultaneously and they interact with each other. We demonstrated that
a multi-layer perceptron can also successfully learn control policies
with policy sharing between multiple agents.

A snapshot of our game is shown in
Fig.\ref{fig:game}. At each time stamp, each car chooses an action
to act based on its state. A reward comes after the execution of
action, which is used to evaluate the transition.
\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{res/game.png}
  \caption{A snapshot of our game. The black solid line represent
    walls. The gray line with direction represents the suggested
    route. The circles represent the cars(agent). The lines attached
    to nodes represent sensors and the visual extent. }
  \label{fig:game}
\end{figure}

\section{Background} \label{sec:background}
We consider tasks in which agents interact with an environment $E$, in
a sequence of actions, observations, and rewards. At each time stamp
$t$, each agent selects an action $a_t$ from a set of legal actions,
$A={1,2,...K}$. The action is passed to the environment and the
internal state will be modified. The internal state is not observable
to the agent. The agent will only receive a vector of sensory data $x_t$ and
a reward $r_t$.

The goal of the agent is to interact with the environment by selecting
actions in a way that maximizes its future reward. We assume that
future rewards are discounted by a factor of $\gamma$ every time
step. We define the {\em return} as
$R_t = \sum_{t'=t}^{T}\gamma^{t'-t} r_{t'}$. We also define an optimal
action-value function
$Q^*(s,a) = \textrm{max}~\mathbb{E} [R_t | s_t=t, a_t=a, \pi]$, where
$\pi$ is a policy that maps sequences to a distribution of actions. If
we know the optimal value $Q^*(s',a')$ of the sequence $s'$ at the
next time stamp $t'$, it will be straight-forward to find the optimal
strategy for time $t$, because
\begin{equation}
Q^* (s,a) = \mathbb{E}_{s' \sim E} [r + \gamma \textrm{max}_{a'} Q^*(s',a') | s, a]
\end{equation}
This equation is also known as {\em Bellman equation}.Theoretically,
by applying the Bellman equation, $Q_{i+1}(s,a) = \mathbb{E} [r+\gamma
\textrm{max}_{a'}Q_i(s',a')|s,a]$, $Q_i$ will converge to $Q^*$ as
$i\rightarrow \infty$. In practice, this is not a feasible solution
because $Q$ is estimated separately for each sequence, which results
in no generalization.

To provide such generalization, people typically use a linear function
to approximately estimate the action-value function,
$Q(s,a;\theta) \approx Q^*(s,a)$, where $\theta$ denotes
weights. Instead of linear functions, we can also use a non-linear
function, for example, a multi-layer perceptron. We can train the network
by minimizing a sequence of loss functions $L_i(\theta_i)$ that
changes at each iteration $i$,
\begin{equation}
L_i(\theta_i) = \mathbb{E}_{s,a\sim \rho(\cdot)} [(y_i - Q(s,a;\theta_i))^2]
\end{equation}
where
$y_i = \mathbb{E}_{s'\sim E}[r + \gamma \textrm{max}_{a'}
Q(s',a';\theta_{i-1}|s,a)]$
is the target for iteration $i$ and $\rho$ is a probability
distribution over sequences $s$ and actions $a$, which is also
referred to as {\em behavior distribution}. The fact that the
targets depend on the network weights is an important difference that
separates reinforcement learning with supervised
learning. We can compute the derivative of loss function with respect
to the weights,
\begin{equation}
\nabla_{\theta_i} L_i(\theta_i) = \mathbb{E}_{s,a \sim
  \rho(\cdot);s' \sim E} \Big[\Big( r + \gamma \textrm{max}_{a'}
Q(s',a';\theta_{i-1}) - Q(s,a;\theta_i) \Big) \nabla_{\theta_i}Q(s,a;\theta_i)\Big]
\end{equation}
Instead of computing full expectations, we do stochastic gradient
descent by randomly taking samples of $(s, a, s')$. {\em Essentially,
  this boils down to iteratively a neural network for regression,
  whose targets depend on the weights from last iteration. We compute
  targets on the fly according to the equations above. }

When there are more than one agent, the only difference is that now we
have to make sure the $s$ and $s'$ we draw as $(s,a,s')$ belong to the
same agent. We will talk more about implementation details in
Section~\ref{sec:detail}.

\section{Prior work} \label{sec:prior}
% previous works
%(citations to previous work in the area, and previous relevant work
%in machine learning algorithms including any reading outside the
%textbook that you found useful, and including citations to any
%resources (code or otherwise) that you used in the project)

Like other applications, i.e. vision and speech, the successful
examples of reinforcement learning used to heavily rely on the quality
of hand-crafted features. Recently, we witnessed a list of remarkable
performance of deep learning in supervised learning, which extracts
high-level features directly from raw sensory data. While, it remains
a question if the success we see in supervised learning can transfer
to reinforcement learning. Despite different kinds of labels
(discrete/continuous label v.s. scalar reward) and assumption of
independence (independent v.s. highly correlated),
\cite{mnih2013playing} proved that a convolutional neural network can
overcome the challenges with the use of (1) a variant of
Q-learning\cite{watkins1992q}; (2) experience replay
mechanism\cite{lin1993reinforcement}.

For reinforcement learning with multiple agents, \cite{tan1993multi}
investigated if cooperative agents who share instantaneous
information, episodic experience, and learned knowledge will
outperform independent agents who do not communicate during
learning. They concluded that {\em if cooperation is done
  intelligently, each agent can benefit from it.} In this paper, we
will also explore if {\em cooperation} will help cars better achieve
their individual goals, in the sense of sharing learned policies but
with individual episodic experiences.

In terms of implementation, we used
ConvNetJS\cite{karpathy2014convnetjs}, which is a Javascript library
for training Deep Learning models entirely in the browser. It does not
have compiling or installation, neither software requirements (BLAS
etc.) and hardware requirements (GPU). It also provides simple yet
powerful features for reinforcement learning, which are missing in
most machine learning libraries. Since we are looking at a rather
simple game, training usually finishes within an hour on CPUs. There
remains some future work in order to apply to more complicated
scenarios.

\section{Project breakdown} \label{sec:breakdown}
% your decomposition of the project into separate portions including
% all responsibilities and credits by group member, pointing external
% code used in each portions, and any milestones or checkpoints you
% used;
The project contains three parts: (1) algorithm (including agent part
and environment part); (2) visualization (live demo); (3) paper
writing. In terms of responsibilities, Peiyun Hu focus on (1) and (3)
and Junzhi Wu is mainly responsible for (1) and (2).

\section{Implementation details} \label{sec:detail}
We use a deep learning library called ConvNetJS for implementation.
ConvNetJS has a tutorial available on its website, but the library is
more than what is in there. It actually provides many more
off-the-shelf implementations for optimization, visualization etc. It
is worthwhile to go through the source code to avoid reinventing the
wheel. For example, Javascript does not have permissions such as file
writing, which we need to save trained models. To get around this, we
followed what ConvNetJS does, which writes model weights to HTML
widgets. This is also useful for dumping raw data when generating
plots.

We define the distance agents traveled within each time stamp to be
the projected distance on the suggested route. We starts with a simple
scenario where there is only one agent, and the suggested route is a
rectangle loop. After a number of iterations, we found the average
reward over time has a very large variance. The reason is that the
reward of distance projection is not smooth at the corner. Ideally, we
would like a route with continuous curvature so that the reward
function becomes continuous as well, but that will require much more
computation. In fact, most of the time, the reward is noisy in
real-world reinforcement learning applications. To just alleviate this
problem a little bit, we adjusted the corner so that the maximum of
angle change becomes 45 degree from the original 90 degree. We plot
the difference of reward function between using 45 degree corner and
90 degree corner in Fig.~\ref{fig:corner}. One more important detail
is in order to make sure the model learns both collision avoidance and
route navigation, we empirically explored how to re-weight these two
factors.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{res/reward_90degree.png}
    \caption{90 degree}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{res/reward_45degree.png}
    \caption{45 degree}
  \end{subfigure}
  \caption{The difference of reward function between using 90 degree
    corner and 45 degree corner. }
  \label{fig:corner}
\end{figure}

Since we have multiple sources for reward function, which are collision
avoidance and route navigation, how each factor is weighted determines
how future behavior the model predicts. We empirically explored the
possibilities of re-weighting these two factors so that the car focus
on both following the direction and avoiding collision. We penalized
going backwards and collision

We define the action as 2d vector of the speed of each wheel. This
essentially reflects which direction the car is moving. To make it
simple, we did not consider acceleration. In our case, we defined 5
actions, which are pointing at [90,37,0,-37,-90]. Note that there are
no sensors on the back, which is the reason why once it starts going
the opposite way it will not stop until it sees other agents coming
too close in front.

We use multi-layer perceptrons to approximate the action-value
function Q*(s, a). We start with a two-layer perceptron and it works
fine when there is only one agent. We found out we need deeper network
when there are more agents involved in the game. We show the final
architecture that we use across the experiments in
Fig.~\ref{fig:net}. The input is a 43-d vector that represents a state
and the output is 5-d vector that represents the value for each
action. The state can be considered as a package of current sensory
status and a history of both sensory status and actions. In our case,
we use 9 distance sensors, 1 angle sensor, 5 actions, and a temporal
window of 1, which adds up to 1*(9*2+1+5)+9*2+1=43 dimensional
input. We use rectified linear units for non-linearity. One important
detail is that we normalize all sensory input (both distance and
angle) to [0,1] to avoid ill-posed optimization problem.

At time stamp $t$, we gather new sensory status from
agents, then perform a forward pass within the network to compute
value for each action. To determine the final action, we apply the
epsilon-greedy policy, which forces agent to perform a random action
at a probability of $\epsilon$ despite the optimal predicted by the
network. The epsilon-greedy helps the agent avoid stucking at reward
local minima. In terms of $\epsilon$, we start with 1.0 and gradually
decrease down to 0.05 as we approach the burn-in size, which we set to
be 3000. During testing, we set the $\epsilon$ to be a constant scalar
0.05. After agents receive and perform the actions, we gather
rewards. Now we have both the state transitions and corresponding
rewards. Then we run backward pass to adjust model weights so that it
maximize the cumulative future reward, which is the sum of discounted
rewards of all future time stamps. The discount factor $\gamma$ we use
is 0.7. Note that we did not fully explore all possibilities of
parameters, so one may find a better set of parameters that fits the
application.

To train the network, we will first fill up a replay
memory, where we store experiences of the state transitions and
corresponding rewards. Once the size of replay memory is above a
threshold, which we set to 30000, we randomly sample a batch of
experiences and use gradient descent to update the model weights. For
gradient descent, we use a batch size of 64, learning rate of 0.01 and
a L2 weight decay of 0.01. When training models for more than one
agent, we initialize the model using a pre-trained model with most
agents, following the intuition ``Introducing gradually more difficult
examples speeds-up online training''\cite{bengio2009curriculum}.

\begin{figure}
  \centering
  \includegraphics[width=.8\linewidth]{res/net.png}
  \caption{The architecture of multi-layer perceptron}
  \label{fig:net}
\end{figure}
% your experience in coding it up

\section{Demo}
% your experience and results including quantitative results in
% testing it, with relevent quantitative comparisons (plots go here!)
Please open the \texttt{racer\_demo.html} within your browser, where
we will demonstrate our models trained under different number of
agents. By default, a model trained with 30 agents will be loaded and
30 agents will be randomly placed around the left top corner of the
2D game world. The demo contains four parts, which are introduced in
details below.

\begin{figure}
  \centering
  \includegraphics[width=0.2\linewidth]{res/input.png}
  \caption{A snapshot of sensory information from a random agent}
  \label{fig:input}
\end{figure}

\subsection{Model specs}
You will see a list of specs on top of the demo. Within the text box,
you should be able to tune \texttt{num\_agents} on the first line,
which controls the number of agents in the play. The new specs will
take effect after the \texttt{Reload} button is clicked.

\subsection{Live visualization}
After the model specs, we show a live visualization in the middle
part, which includes (1) real-time sensory information from a random
agent (as in Fig.~\ref{fig:input}); (2) average reward function over
time (as in Fig.~\ref{fig:corner}); (3) activation inside the network
(as in Fig.~\ref{fig:net}); (4) game play (as in
Fig.~\ref{fig:game}). Note that it takes several seconds to stabilize
from the random initialization.

\subsection{Control options}
Further down, you will see a list of control options,
where you can control the speed of game play, and switch learning
on/off.

\subsection{Pre-trained models}
At the bottom, we listed several pre-trained models, which are tuned
with different numbers of agents. Even though we provide models tuned
with a certain number of agents, each of them is able to play with
less agents. The intuition the model tuned with more agents is more
robust to smaller spacing. The observation verifies that a model
trained with curriculum learning is able to generalize to easier
scenarios.

\section{Future work}
This paper focus on qualitative results only, there are yet at least questions
remain to be solved:
\begin{enumerate}
\item What is the best way to measure performance of this system
  quantitatively? (number of crashes within certain amount of time, or
  time steps to finish certain distance
\item How much does multi-agent learning gain from knowledge sharing
  comparing to independent learning?
\item Does curriculum learning speeds up convergence or improve the
  final performance?
\end{enumerate}

\section{Conclusions}
We discussed how to use multi-layer perceptron to estimate the
action-value function in reinforcement learning. We demonstrated its
ability to master difficult control policies from raw sensory
information for a simple navigation task. We implemented both
single-agent and multi-agent variants of the algorithm. Experiment
results showed that the learning works with different numbers of
agents.

\bibliographystyle{plain}
\bibliography{ref}

\newpage
\appendix
\section{More experiments}
We experimented with a much more difficult scenario, in which we
intentionally include asymmetry, traps, ambiguity. The route is
visualized in Fig.~\ref{fig:hard}. We has not finished training, yet
the reward function is gradually going up while having large variance
as shown in Fig.~\ref{fig:reward2}.
\begin{figure}
  \centering
  \includegraphics[width=.8\linewidth]{res/hard.png}
  \caption{A much harder route configuration}
  \label{fig:hard}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.6\linewidth]{res/reward_hard.png}
  \caption{A much harder route configuration}
  \label{fig:reward2}
\end{figure}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
